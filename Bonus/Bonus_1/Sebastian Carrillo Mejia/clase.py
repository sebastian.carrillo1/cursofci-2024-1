import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class parabolic:   
    def __init__ (self, angle: float, initial_velocity: float, initial_heigth: float, initial_x:float) -> None:
        '''
        Given some initial conditions for the parabolic movement, we define the properties of the object.
        
        args:
        - angle: is a float, ninitial angle in degrees 
        - initial_velocity: is the modulous of the initial velocity of movement.
        - initial height: float, intial height of movement
        - inicial_x: is the position on x from which the movement starts.
        '''
        self.Theta = angle * np.pi /180
        self.v0 = initial_velocity
        self.y0 = initial_heigth
        self.x0 = initial_x
        self.g:float = 9.8 
        self.total_time:float = ( -self.v0 * np.sin(self.Theta) - ( (self.v0 * np.sin(self.Theta))**2 + 2 * self.g * self.y0)**(0.5) ) / ( -self.g )
        self.t:float = np.arange(0,self.total_time,0.001)
    
    
    
    
    def trajectoryY(self):
        '''
        This method, calculates the Y trayectory of an object given some initial conditions defined in the constructor supposing 
        constant acceleration.
        
        returns:
        - Velocity at all time in x and y directions
        - Total time of movement
        - position x and y
        - plots the position.
        -Saves the information in a csv file.
        '''
        
        
        #calculating position.
        y:np.ndarray = self.y0 + self.v0 * np.sin(self.Theta) * self.t - 0.5 * self.g * self.t*self.t
        
        #ecuations for velocity
        velocity_y:np.ndarray = self.v0 * np.sin(self.Theta) - self.g * self.t
        
        return y,velocity_y
    
    def trajectoryX(self):
        '''
        This method, calculates the X trayectory of an object given some initial conditions defined in the constructor supposing 
        constant acceleration.
        
        returns:
        - Velocity at all time in x and y directions
        - Total time of movement
        - position x and y
        - plots the position.
        -Saves the information in a csv file.
        '''
        
        #calculating position.
        
        x:np.ndarray = self.x0 + self.v0 * np.cos(self.Theta) * self.t 
        #ecuations for velocity
        velocity_x:np.ndarray = self.v0 * np.cos(self.Theta) 
        
        return x, velocity_x
    
    
    
    def plot(self, name:str,x:list,y:list,velocity_x:list,velocity_y:list ):
            
        #Ploting the results
        fig = plt.figure(figsize=[10,7])
        plt.plot(x,y,'--k',label = 'Trajectory')
        plt.plot(x[-1],y[-1],'co' ,label='Final position')
        
        plt.ylabel(f'$y(m)$')
        plt.xlabel(f'$x(m)$\n\n cond: v_0= {self.v0}m/s,  x_0={self.x0}m,  y_0= {self.y0}m,  theta= {np.round(self.Theta,3)}rad,  total_time={np.round(self.total_time,4)}s')
        
        plt.title('Position of particle for parabolic movement.\n')
        
        
        plt.legend(loc = 'best')
        plt.grid(True)
        plt.savefig(f'{name}.png', dpi = 250, format = 'png' )
        
        #saving the data in a csv file.
        data = pd.DataFrame(
            {'x (m)': x, 
             'y (m)': y,
             'vx (m/s)': velocity_x,
             'vy (m/s)': velocity_y,
             'total_time (s)': self.total_time
            }).reset_index()
        data.to_csv('data_'+ name +'_.csv' , sep =',', index= False, encoding='utf-8')
        
    
        
        
        
class secondParabolic(parabolic):
    def __init__(self, angle: float, initial_velocity: float, initial_heigth: float, initial_x:float,x_acc:float):
        super().__init__(angle, initial_velocity, initial_heigth, initial_x)  
        self.ax = x_acc
    def trajectoryX(self):
        '''
        This method, calculates the X trayectory of an object given some initial conditions defined in the constructor supposing 
        constant acceleration.
        
        returns:
        - Velocity at all time in x and y directions
        - Total time of movement
        - position x and y
        - plots the position.
        -Saves the information in a csv file.
        '''
        #calculating position.
        
        x:np.ndarray = self.x0 + self.v0 * np.cos(self.Theta) * self.t + 0.5 * self.ax * self.t*self.t
        #ecuations for velocity
        velocity_x:np.ndarray = self.v0 * np.cos(self.Theta) + self.ax * self.t
        return x, velocity_x
    
        
    
        
        

