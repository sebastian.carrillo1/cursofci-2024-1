from clase import parabolic, secondParabolic 

if __name__ == '__main__':
    '''
    Here we create an object fro the class parabolic, from that class we use the methos trajectory to
    calculate the trajectory of the parabolic movement given the initial conditions.
    '''
    objeto:object = parabolic(angle=45, initial_velocity=5, initial_heigth=5, initial_x=3)
    objeto.plot(
        name='trajectory',
        x = objeto.trajectoryX()[0],
        y = objeto.trajectoryY()[0],
        velocity_x=objeto.trajectoryX()[1],
        velocity_y=objeto.trajectoryY()[1])
    
    objeto2:object = secondParabolic(angle=45, initial_velocity=5, initial_heigth=5, initial_x=3, x_acc= -6)
    objeto2.plot(name='trajectory2',
        x = objeto2.trajectoryX()[0],
        y = objeto2.trajectoryY()[0],
        velocity_x= objeto2.trajectoryX()[1],
        velocity_y= objeto2.trajectoryY()[1])