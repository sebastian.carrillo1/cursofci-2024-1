import numpy as np
import matplotlib.pyplot as plt

class tiroParabolico():

    def __init__(self, theta, v, h0, x0,g):

        self.theta = theta * (np.pi/180) # Conversión de grados a radianes
        self.v = v
        self.h0 = h0
        self.x0 = x0
        self.g = g

    def vueloTiempo(self):
        theta = self.theta
        v = self.v
        g = self.g
        y0 = self.h0
        t = (v*np.sin(theta)+np.sqrt((v*np.sin(theta))**2-4*(0.5*g)*y0) )/(-g)
        return t
    
    def yVelocidad(self,t):
        return (self.v*np.sin(self.theta)+ self.g*t)
    

    def xPos(self,t):
        return self.x0+self.v*np.cos(self.theta)*t
    
    def yPos(self,t):
        return self.h0+self.v*np.sin(self.theta)*t+(0.5)*self.g*(t**2)
    
    def posGraph(self,t):
        t = np.linspace(0,t,30)
        y = self.yPos(t)
        x = self.xPos(t)

        fix, ax = plt.subplots(1,3,figsize=(16,5))

        ax[0].set_title('Grafica de la posición en x vs t')
        ax[0].plot(t,x, color='red')
        ax[0].set_xlabel('t (s)')
        ax[0].set_ylabel('x(m)')
        ax[0].grid(True)
        ax[1].set_title('Grafica de la posición en y vs t')
        ax[1].plot(t,y, color='red')
        ax[1].set_xlabel('t (s)')
        ax[1].set_ylabel('y(m)')
        ax[1].grid(True)
        ax[2].set_title('Grafica de la trayectoria')
        ax[2].plot(x,y)
        ax[2].set_xlabel('x (m)')
        ax[2].set_ylabel('y(m)')
        ax[2].grid(True)
        plt.savefig('grafica.png')